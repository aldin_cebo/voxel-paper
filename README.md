# README #


### What is this repository for? ###

Please see `documentation.pdf` file for general pipeline.

### How do I get set up? ###

Install Blender: https://www.blender.org/.

### Who do I talk to? ###

Please see corresponding author in the paper.
