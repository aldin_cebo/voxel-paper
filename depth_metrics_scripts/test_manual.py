import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.path import Path
import PIL.Image as pil
import cv2


class FigureWrapper(object):
    '''Frees underlying figure when it goes out of scope. 
    '''

    def __init__(self, figure):
        self._figure = figure

    def __del__(self):
        plt.close(self._figure)

# --------------------------------------------------------
def rmse_metric(x,y):

    z = (x-y)**2
    z = np.mean(z)
    z = np.sqrt(z)

    return z
    
def l1_metric(x,y):

    z = x-y
    z = np.abs(z)
    z = np.mean(z)

    return z

def log10_metric(x,y):

    def log10(x):
        """Convert a new tensor with the base-10 logarithm of the elements of x. """
        return np.log(x) / np.log(10)

    z = log10(x) - log10(y)
    z = np.abs(z)
    z = np.mean(z)

    return z

def absrel_metric(x,y):

    z = x - y
    z = z/y
    z = np.abs(z)
    z = np.mean(z)

    return z

def delta123_metric(x,y):

    maxRatio = np.maximum(np.divide(x,y), np.divide(y,x))
        
    delta1 = np.mean(maxRatio < 1.25 ** 1)
    delta2 = np.mean(maxRatio < 1.25 ** 2)
    delta3 = np.mean(maxRatio < 1.25 ** 3)

    return delta1, delta2, delta3

# --------------------------------------------------------
def create_mask(shape, poly_verts):
    ny, nx = shape

    # Create vertex coordinates for each grid cell...\n",
    # (<0,0> is at the top left of the grid in this system)\n",
    x, y = np.meshgrid(np.arange(nx), np.arange(ny))
    x, y = x.flatten(), y.flatten()

    points = np.vstack((x,y)).T

    path = Path(poly_verts)
    grid = path.contains_points(points)
    grid = grid.reshape((ny,nx))

    return grid

def get_lidar_depth(path):

    depth = pil.open(path)
    return np.array(depth).astype(np.float32) / 256

def get_voxel_depth(path):
    
    path = path.replace('png', 'exr')

    depth = cv2.imread(path,  cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    return depth[:,:,0]

# --------------------------------------------------------

if __name__ == '__main__':
    

    add_correction_KITTI_GT = False
    
    polygon = [
        [124, 372],
        [470, 236],
        [533, 229],
        [567, 194],
        [606, 194],
        [794, 370]
    ]

    path_gt     = os.path.join('gt', '2011_09_26_drive_0009_sync', '0000000020.png')
    # path_vox    = os.path.join('vox', '2011_09_26_drive_0009_sync_eevee', '0000000020.exr')
    # path_vox    = os.path.join('vox', '2011_09_26_drive_0009_sync_remesh_L_eevee', '0000000020.exr')
    # path_vox    = os.path.join('vox', '2011_09_26_drive_0009_sync_cycles', '0000000020.exr')
    path_vox    = os.path.join('vox', '2011_09_26_drive_0009_sync_remesh_L_cycles', '0000000020.exr')

    # Load all
    depth_gt    = get_lidar_depth(path_gt)
    depth_vox   = get_voxel_depth(path_vox)

    if add_correction_KITTI_GT:
        depth_gt = np.roll(depth_gt, -1, axis=0)
        depth_gt = depth_gt[1:-1,:]
        depth_vox = depth_vox[1:-1,:]

    # Create mask of interest
    mask = create_mask(depth_gt.shape[:2], polygon)

    # Lidar mask
    mask_gt = depth_gt > 0

    # Voxel mask
    mask_vox = depth_vox < 1000

    # Final mask
    mask = np.bitwise_and(mask, mask_gt)
    mask = np.bitwise_and(mask, mask_vox)

    depth_gt_masked = np.ma.masked_where(mask == False, depth_gt)
    depth_vox_masked = np.ma.masked_where(mask == False, depth_vox)


    data_lidar = depth_gt_masked.flatten()
    data_voxel = depth_vox_masked.flatten()

    # Metrics

    rmse = rmse_metric(data_lidar, data_voxel)
    l1 = l1_metric(data_voxel, data_lidar)
    log10 = log10_metric(data_voxel, data_lidar)
    absrel = absrel_metric(data_voxel, data_lidar)
    delta123 = delta123_metric(data_voxel, data_lidar)


    print('RMSE: ', rmse)
    print('L1: ', l1)
    print('Log10: ', log10)
    print('AbsRel: ', absrel)
    print('Delta123: ', delta123)

    # ----------------------------
    # Plot

    # ------------------
    plt.imshow(np.sqrt((depth_gt_masked - depth_vox_masked)**2))
    # plt.imshow(depth_gt_masked)
    plt.colorbar()
    plt.show()

    # -----------------

    data_lidar = depth_gt_masked[mask].flatten()
    data_voxel = depth_vox_masked[mask].flatten()

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
    # fig.subplots_adjust(hspace=0.3)

    fig.suptitle('RMSE: {0:.2f}, L1: {1:.2f}, Log10: {2:.2f}, AbsRel: {3:.2f}, d1: {4:.2f}, d2: {5:.2f}, d3: {6:.2f}'.format(rmse, l1, log10, absrel, delta123[0], delta123[1], delta123[2]), fontsize=10)

    # Plot limits
    point1 = [0, 0]
    point2 = [60, 60]

    # Scatter
    ax1.scatter(data_lidar, data_voxel, marker='.')
    # ax1.hist2d(data_lidar, data_voxel, bins=(100, 100), range=((point1[0], point2[0]), (point1[1], point2[1])))
    ax1.plot([point1[0], point2[0]], [point1[1], point2[1]], color='grey')
    # ax1.set_xlabel('lidar [m]')
    ax1.set_ylabel('voxel [m]')
    ax1.set_xlim(point1[0], point2[0])
    ax1.set_ylim(point1[1], point2[1])

    # Ratio 
    ratio = data_voxel/data_lidar
    # ax2.scatter(data_lidar, ratio, marker='.')
    ax2.hist2d(data_lidar, ratio, bins=(100, 100), range=((point1[0], point2[0]), (0.8, 1.2)))

    ax2.set_ylabel('voxel/lidar')
    ax2.set_xlabel('lidar [m]')

    ax2.set_xlim(point1[0], point2[0])
    ax2.set_ylim(0.8, 1.2)

    fig.savefig('test.png')

    fig_xy = FigureWrapper(fig)
