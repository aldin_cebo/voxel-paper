import os
import matplotlib.pyplot as plt

class FigureWrapper(object):
    '''Frees underlying figure when it goes out of scope. 
    '''

    def __init__(self, figure):
        self._figure = figure

    def __del__(self):
        plt.close(self._figure)
        # print("Figure removed")

if __name__ == '__main__':

    with open('camera_pose.txt', 'r') as f:

        data = f.read().splitlines()

    number_of_frames = len(data)

    x_min = -300
    x_max = 300

    y_min = -300
    y_max = 300

    z_min = 0
    z_max = 50

    # h = 370
    # w = h

    h = 375
    w = h

    my_dpi = 80

    for f in range(number_of_frames):

        # if f > 1:
        #     break

        print(f)
        fig_xy = plt.figure(figsize=(w/my_dpi, h/my_dpi), dpi=my_dpi)
        ax_xy = fig_xy.add_subplot(1,1,1)


        ax_xy.set_xlim(x_min, x_max)
        ax_xy.set_ylim(y_min, y_max)
        # ax_xy.set_xlabel('X [m]')
        # ax_xy.set_ylabel('Y [m]')

        # fig_z, ax_z = plt.subplots(figsize=(w/my_dpi, h/my_dpi), dpi=my_dpi)
        # fig_z_wrapped = FigureWrapper(fig_z)        
        # ax_z.set_xlim(0 - 1, number_of_frames+1)
        # ax_z.set_ylim(z_min, z_max)
        # ax_z.set_xlabel('Frame number')
        # ax_z.set_ylabel('Altitude [m]')

        for d in data[:f+1]:

            frame, x, y, z = d.split(',')
            frame, x, y, z = int(frame), float(x), float(y), float(z)

            ax_xy.plot(x, y, 'b.')
            # ax_z.plot(frame, z, 'b.')

        # ax_z.grid()
        ax_xy.grid()

        fig_xy.savefig(os.path.join('xy', '{:04d}.png'.format(frame)), dpi=my_dpi)
        # fig_z.savefig(os.path.join('z', '{:04d}.png'.format(frame)), dpi = my_dpi)

        fig_xy = FigureWrapper(fig_xy)