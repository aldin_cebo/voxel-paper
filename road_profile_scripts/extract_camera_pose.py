# Command example
# /Applications/Blender.app/Contents/MacOS/Blender /Users/benjamin/Documents/data/KITTI/kitti_paper/2011_09_30_drive_0033_sync_vox/2011_09_30_drive_0033_sync_vox.blend --background --python extract_camera_pose.py 

import bpy

scene = bpy.context.scene
the_camera = scene.objects['Camera 2']

with open('camera_pose.txt', 'w') as f:

    for i in range(1594):

        bpy.context.scene.frame_set(i)

        print(i, the_camera.location.x, the_camera.location.y, the_camera.location.z)
        f.write('{}, {}, {}, {} \n'.format(i, the_camera.location.x, the_camera.location.y, the_camera.location.z))
