from PIL import Image
from PIL.ExifTags import TAGS
import os
import piexif
import math


def gps_to_exif_dict(lat, lon, alt):
    alt_top = math.trunc(alt*1000)
    deg_lat_top = math.trunc(lat)
    min_lat = math.modf(lat)[0]*60
    min_lat_top = math.trunc(min_lat)
    sec_lat_top = math.trunc(math.modf(min_lat)[0]*60000000)
    deg_lon_top = math.trunc(lon)
    min_lon = math.modf(lon)[0]*60
    min_lon_top = math.trunc(min_lon)
    sec_lon_top = math.trunc(math.modf(min_lon)[0]*60000000)

    return {
        piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
        piexif.GPSIFD.GPSLatitudeRef: "N",
        piexif.GPSIFD.GPSLatitude: ((deg_lat_top, 1), (min_lat_top, 1), (sec_lat_top, 1000000)),
        piexif.GPSIFD.GPSLongitudeRef: "E",
        piexif.GPSIFD.GPSLongitude: ((deg_lon_top, 1), (min_lon_top, 1), (sec_lon_top, 1000000)),
        piexif.GPSIFD.GPSAltitudeRef: 0 if alt_top > 0 else 1,
        piexif.GPSIFD.GPSAltitude: (abs(alt_top), 1000)
    }


path_images = "F:/Work/KITTI-paper/data/2011_09_26_drive_0009_sync/image_03/data"
path_oxts = "F:/Work/KITTI-paper/data/2011_09_26_drive_0009_sync_vox/oxts_per_camera/03" 
path_dest = "F:/Work/KITTI-paper/data/2011_09_26_drive_0009_sync_vox/image_exif/03"


files_oxts = sorted([os.path.join(path_oxts, f) for f in os.listdir(path_oxts) if ".txt" in f])
files_images = sorted([os.path.join(path_images, f) for f in os.listdir(path_images) if ".png" in f])

for i in range(0, len(files_oxts)):
    file_oxts = open(files_oxts[i], "r")
    data_oxts = file_oxts.read().split(" ")
    lat = float(data_oxts[0])
    lon = float(data_oxts[1])
    alt = float(data_oxts[2])

    exif_gps = gps_to_exif_dict(lat, lon, alt)
    exif_dict = {
        "GPS":exif_gps 
    }

    exif_bytes = piexif.dump(exif_dict)

    image = Image.open(files_images[i])
    image.save(path_dest + "/" + (f"{i:09}") + ".jpg", exif=exif_bytes)

