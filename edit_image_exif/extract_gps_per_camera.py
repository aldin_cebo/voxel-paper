# CONVENTIONS
# y-positive is north, x-positive is east
# scale = 1.0
# all coordinates are expressed in global - imu location at frame 0 is (0,0,0), rotation is (0,0,0)
# cameras have a constant offset regarding the imu.
# in order to have the first camera at (0,0,0) a translation and rotation around the world origin must be done.


import bpy
import math
import os


#absolute path to oxts folder for current scene
path_oxts = "F:/Work/KITTI-paper/data/2011_09_26_drive_0009_sync/oxts/data" 
files_oxts = sorted([os.path.join(path_oxts, f) for f in os.listdir(path_oxts) if ".txt" in f])

export = True
# path for output of modified gps coordinates
path_dest = "F:/Work/KITTI-paper/data/2011_09_26_drive_0009_sync_vox/oxts_per_camera_2"

# make sure that directories exist
for dirname in ["00", "01", "02", "03"]:
    if not os.path.exists(path_dest + "/" + dirname):
        os.makedirs(path_dest + "/" + dirname)

# load first frame and use first frame position as reference (0,0,0)
with open(files_oxts[0], "r") as file_oxts:
    data = file_oxts.read().split(" ")
    lat_0 = float(data[0])
    lon_0 = float(data[1])
    alt_0 = float(data[2])

# write lambdas for lat/lon to x/y conversion
r = 6378137 # Earths radius in meters
s = math.cos(lat_0*math.pi/180)
lon_to_x = lambda lon: s*r*math.pi*lon/180
lat_to_y = lambda lat: s*r*math.log(math.tan(math.pi*(90+lat)/360))

x_to_lon = lambda x: (x*180)/(s*r*math.pi)
y_to_lat = lambda y: (360/math.pi) * math.atan(math.exp(y/(s*r))) - 90

# initial location
x_0 = lon_to_x(lon_0)
y_0 = lat_to_y(lat_0)
z_0 = alt_0

# get rig and cameras
rig_obj = bpy.data.objects["rig"]
cameras = [bpy.data.objects["cam_0_(gray)"], bpy.data.objects["cam_1_(gray)"], bpy.data.objects["cam_2_(color)"], bpy.data.objects["cam_3_(color)"]]

# iterate over all oxts files move the rig and insert keyframe
for i, filepath in enumerate(files_oxts):
    with open(filepath, "r") as file_oxts:
        data = file_oxts.read().split(" ")
        
        lat = float(data[0])   # deg
        lon = float(data[1])   # deg
        alt = float(data[2])   # m
        roll = float(data[3])  # rad
        pitch = float(data[4]) # rad
        yaw = float(data[5])   # rad
        
        x = lon_to_x(lon) - x_0
        y = lat_to_y(lat) - y_0
        z = alt - z_0

        rig_obj.rotation_euler = (roll, pitch, yaw)
        rig_obj.location = (x, y, z)
        rig_obj.keyframe_insert(data_path="location", frame=i)
        rig_obj.keyframe_insert(data_path="rotation_euler", frame=i)
        bpy.context.scene.frame_set(i)
        # export gps into separate oxts files for each camera
        if export:
            for j in range(0, 4):
                path = path_dest + "/0" + str(j) + "/" + (f"{i:010}") + ".txt"
                
                trans_vec = cameras[j].matrix_world.to_translation()
                lat = y_to_lat(trans_vec[1]+y_0)
                lon = x_to_lon(trans_vec[0]+x_0)
                alt = trans_vec[2]+z_0

                f = open(path, "w")
                f.write(" ".join(str(ele) for ele in [lat, lon, alt, roll, pitch, yaw]))
                f.close()