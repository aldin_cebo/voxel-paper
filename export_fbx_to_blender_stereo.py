# /Applications/Blender.app/Contents/MacOS/Blender --background --python export_fbx_to_blender_stereo.py

import bpy
import os
import numpy as np

def prep_new_camera(the_camera, list_of_RC_cameras):

    # Location
    for n,f in enumerate(list_of_RC_cameras):

        xyz = f.location.xyz

        the_camera.location.x = xyz.x
        the_camera.location.y = xyz.y
        the_camera.location.z = xyz.z
        the_camera.keyframe_insert(data_path="location", frame=n)

    # Rotation
    for n,f in enumerate(list_of_RC_cameras):

        rot = f.rotation_euler

        the_camera.rotation_euler.x = rot.x
        the_camera.rotation_euler.y = rot.y
        the_camera.rotation_euler.z = rot.z
        the_camera.keyframe_insert(data_path="rotation_euler", frame=n)

    # Camera lens
    for n,f in enumerate(list_of_RC_cameras):

        rot = f.rotation_euler

        the_camera.data.lens = f.data.lens
        the_camera.data.keyframe_insert(data_path="lens", frame=n)


# ----------------------------------
# CONFIG
image_height = 375
image_width = 1242

# ----------------------------------
# import file
fbx = bpy.ops.import_scene.fbx(filepath='test_Model_1.fbx')
scene = bpy.context.scene

# remove default objects
bpy.data.objects.remove(scene.objects['Camera'])
bpy.data.objects.remove(scene.objects['Cube'])
bpy.data.objects.remove(scene.objects['Light'])

# setup resolution
scene.render.resolution_x = image_width
scene.render.resolution_y = image_height

# list all RC cameras
cameras = [obj for obj in scene.objects if '.' in obj.name]
number_of_cameras = len(cameras)

# ----------------------------------
# add left and right camera (source: https://blender.stackexchange.com/questions/151319/adding-camera-to-scene)
cam_left = bpy.data.cameras.new("Camera Left")
cam_left_obj = bpy.data.objects.new("Camera Left", cam_left)
scene.collection.objects.link(cam_left_obj)
prep_new_camera(cam_left_obj, cameras[:int(number_of_cameras/2)])

cam_right = bpy.data.cameras.new("Camera Right")
cam_right_obj = bpy.data.objects.new("Camera Right", cam_right)
scene.collection.objects.link(cam_right_obj)
prep_new_camera(cam_right_obj, cameras[int(number_of_cameras/2):])

# ----------------------------------
# Remove old cameras
for n,f in enumerate(cameras):
    bpy.data.objects.remove(f)

bpy.ops.wm.save_as_mainfile(filepath='test.blend')