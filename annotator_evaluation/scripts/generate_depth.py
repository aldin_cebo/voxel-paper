# "C:\Program Files\Blender Foundation\Blender 2.90\blender.exe" "..\annotation\test.blend" --background --python generate_depth.py
# "C:\Program Files\Blender Foundation\Blender 2.90\blender.exe" "..\gt\test.blend" --background --python generate_depth.py

import os
import bpy

# Change as needed
start_frame = 55
end_frame = 105
frame_step = 1
object_name = "car"
is_gt = False

path_annot_output = os.path.join("..", "results")
path_gt_output = os.path.join("..", "gt", "depth")

def main():

    annotator = bpy.path.basename(bpy.context.blend_data.filepath).split(".")[0]

    # ---------------------------------
    # Shortcuts
    bpy.context.scene.use_nodes = True
    scene   = bpy.context.scene
    tree    = scene.node_tree
    links   = tree.links

    # Clear default nodes
    for n in tree.nodes:
        tree.nodes.remove(n)

    scene.render.image_settings.file_format = 'OPEN_EXR'

    # ---------------------------------
    # Prepare nodes

    rl = tree.nodes.new('CompositorNodeRLayers')
    output = tree.nodes.new('CompositorNodeComposite') 
    links.new(rl.outputs[2], output.inputs[0])

    # ---------------------------------
    number_of_frames = scene.frame_end - scene.frame_start + 1

    print('Number of frames: {}'.format(number_of_frames))

    # Get cameras
    cameras = {
        '02'    : scene.objects['Camera 2'],
        # '03'    : scene.objects['Camera 3'],
    }

    if is_gt:
        path_output = path_gt_output
    else:
        path_output = path_annot_output

    # Loop over camera
    for c, bpy_c in cameras.items():

        scene.camera = bpy_c

        # Loop over frames
        for f in range(start_frame, end_frame + 1, frame_step):
            scene.frame_set(f)
            # Extract depth
            scene.render.filepath = os.path.join(os.getcwd(), path_output, annotator, c, object_name, '{:010d}'.format(f))
            bpy.ops.render.render(write_still=True)


if __name__ == "__main__":
    main()