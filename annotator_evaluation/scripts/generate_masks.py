# "C:\Program Files\Blender Foundation\Blender 2.90\blender.exe" "..\annotation\test.blend" --background --python generate_masks.py
# "C:\Program Files\Blender Foundation\Blender 2.90\blender.exe" "..\gt\test.blend" --background --python generate_masks.py

import os
import bpy

# Change as needed
start_frame = 55
end_frame = 105
frame_step = 1
object_name = "car"
is_gt = True

path_annot_output = os.path.join("..", "results")
path_gt_output = os.path.join("..", "gt", "masks")

def main():

    annotator = bpy.path.basename(bpy.context.blend_data.filepath).split(".")[0]

    # ---------------------------------
    # Shortcuts
    bpy.context.scene.use_nodes = True
    scene   = bpy.context.scene

    # ---------------------------------
    # Render setup
    scene.render.image_settings.file_format = 'PNG'
    scene.render.engine = 'BLENDER_EEVEE'
    scene.display_settings.display_device = 'sRGB'
    scene.view_settings.view_transform = 'Raw'
    scene.view_settings.look = 'None'
    scene.view_settings.exposure = 0
    scene.view_settings.gamma = 1
    scene.sequencer_colorspace_settings.name = 'sRGB'
    scene.render.dither_intensity = 0
    scene.render.filter_size = 0.01
    scene.render.use_compositing = False
    bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[0].default_value = (0, 0, 0, 1)

    for col in bpy.data.collections:
        
        if "cameras" in col.name or "raw" in col.name:
            continue
        
        # if col.name in mask_col:
        else:
            col_objects = col.objects[:]
            print ("Mask set on: " + col.name)
            for obj in col_objects:
                if obj.name == object_name:
                    obj.hide_render = False
                    if obj.data.materials:
                        # assign to 1st material slot
                        obj.data.materials[0] = bpy.data.materials["Mask"]
                    else:
                        # no slots
                        obj.data.materials.append(bpy.data.materials["Mask"])
                else:
                    if obj.data.materials:
                        obj.data.materials.clear()
            
        # else:
        #     col_objects = col.objects[:]
        #     for obj in col_objects:
        #         obj.data.materials.clear()

    # ---------------------------------
    number_of_frames = scene.frame_end - scene.frame_start + 1
    print('Number of frames: {}'.format(number_of_frames))

    # Get cameras
    cameras = {
    '02'    : scene.objects['Camera 2'],
    # '03'    : scene.objects['Camera 3'],
    }

    if is_gt:
        path_output = path_gt_output
    elif not is_gt:
        path_output = path_annot_output

    # Loop over camera
    for c, bpy_c in cameras.items():

        scene.camera = bpy_c

        # Loop over frames
        for f in range(start_frame, end_frame + 1, frame_step):
            scene.frame_set(f)
            # Extract depth
            scene.render.filepath = os.path.join(os.getcwd(), path_output, annotator, c, object_name, '{:010d}'.format(f))
            bpy.ops.render.render(write_still=True)


if __name__ == "__main__":
    main()