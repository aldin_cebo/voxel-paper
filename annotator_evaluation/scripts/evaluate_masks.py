import os
import PIL.Image as pil
import numpy as np
import matplotlib.pyplot as plt
from cycler import cycler


def get_mask(path):
    mask = pil.open(path)
    return np.array(mask)


def check_and_load_all_files(path_annot, path_gt):
    files_gt    = set([i.split('.')[0] for i in os.listdir(path_gt) if '.png' in i])
    files_annot  = set([i.split('.')[0] for i in os.listdir(path_annot) if '.png' in i])

    overlap = sorted(set.intersection(files_gt, files_annot))
    return overlap


def main():
    # Change as needed
    # Set annotator_file_name to be the name of the folder, if empty the script will go through all the folders
    annotator_file_name = "test"

    path_res = os.path.join("..", "results")
    path_gt = os.path.join("..", "gt", "masks")

    subfolders = [ f.path for f in os.scandir(path_res) if f.is_dir() ]
    if annotator_file_name:
        subfolders = [s for s in subfolders if annotator_file_name in s]
    for path_person in subfolders:
        cameras = [ f.path for f in os.scandir(path_person) if f.is_dir() ]
        for path_cam in cameras:
            objects = [ f.path for f in os.scandir(path_cam) if f.is_dir() ]

            fig = plt.figure()
            ax1 = fig.add_subplot(111)
            ax1.set_prop_cycle(cycler('color', ['c', 'm', 'y', 'k']))

            for path_annotated_object in objects:
                path_gt_complete = os.path.join(path_gt, os.path.basename(path_person), os.path.basename(path_cam), os.path.basename(path_annotated_object))
                files = check_and_load_all_files(path_annotated_object, path_gt_complete)

                ious = []
                # Loop over all overlaps
                for img in files:
                    # ----------------------------------------
                    # Load depth maps and image
                    img_annot = get_mask(os.path.join(path_annotated_object, img + '.png'))
                    img_gt    = get_mask(os.path.join(path_gt_complete, img + '.png'))

                    axes = (1,2) # W,H axes of each image
                    intersection = np.sum(np.abs(img_annot * img_gt), axis=axes)
                    mask_sum = np.sum(np.abs(img_gt), axis=axes) + np.sum(np.abs(img_annot), axis=axes)
                    union = mask_sum  - intersection
                    smooth = .001
                    iou = (intersection + smooth) / (union + smooth)
                    # dice = 2 * (intersection + smooth)/(mask_sum + smooth)
                    iou = np.mean(iou)
                    # dice = np.mean(dice)
                    ious.append(iou)
            
                ax1.scatter(np.arange(0, len(ious), 1), ious, label=os.path.basename(path_annotated_object))

            plt.legend(loc='lower left')
            # plt.yticks(np.arange(0, 1.05, 0.05))
            ax1.set_ylim([0,1])
            #plt.show()

            # plt.bar(frames, ious, width=0.8)
            # axes = plt.gca()
            # axes.set_ylim([0,1])
            # for tick in axes.xaxis.get_major_ticks():
            #     tick.label.set_fontsize(10)
            # plt.yticks(np.arange(0, 1.05, 0.05))
            # plt.xticks(rotation=90)
            # fig = plt.gcf()
            # fig.set_size_inches(14,8)

            plt.ylabel("IoU")
            plt.title(os.path.basename(path_person) + " - Camera: " + os.path.basename(path_cam))
            plt.savefig(path_cam, dpi=300)
            print("Saved figure: " + path_cam)

if __name__ == "__main__":
    main()